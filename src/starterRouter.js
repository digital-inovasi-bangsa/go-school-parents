import Vue from "vue";
import Router from "vue-router";
import header from "./layout/starter/StarterHeader.vue";
import Register from "./views/Register.vue";
import Match from "./views/Match.vue";
import Parent from "./views/Parent.vue";

Vue.use(Router);

export default new Router({
  routes: [{
      path: "/",
      name: "starter",
      components: {
        default: Register,
      }
    },
    {
      path: "/match",
      name: "match",
      components: {
        default: Match
      }
    },
    {
      path: "/parent",
      name: "parent",
      components: {
        default: Parent
      }
    }
  ]
});